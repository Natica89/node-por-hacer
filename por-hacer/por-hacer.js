//file system
const fs = require('fs');

let listadoPorHacer = [];

//obtener la persistencia
const guardarDB = () => {

    //pasar listadoPorHacer a formato JSON
    let data = JSON.stringify(listadoPorHacer);
    //GUARDAR EN EL file System
    //primer argumento: donde esta la data
    //segundo: la data que se va almacenar
    //tercero: su ocurre un error
    fs.writeFile('db/data.json', data, (err) => {
        if (err) throw new Error('No se puede grabar la data', err);
    });
}

const cargaDB = () => {
    //El requiere convierte el formato JSON a objeto
    try {
        listadoPorHacer = require('../db/data.json');
    } catch (error) {
        listadoPorHacer = [];
    }


}

const crear = (descripcion) => {

    cargaDB();

    let porHacer = {
        descripcion,
        completado: false
    };

    listadoPorHacer.push(porHacer);

    guardarDB();

    return porHacer;

}

//Listado de tareas Por Hacer
const getListado = () => {

    guardarDB();

    return listadoPorHacer;

}

const actualizar = (descripcion, completado = true) => {
    guardarDB();

    let index = listadoPorHacer.findIndex(tarea =>
        tarea.descripcion === descripcion)

    if (index >= 0) {
        listadoPorHacer[index].completado = completado;
        guardarDB();
        return true;
    } else {
        return false;
    }

}

const borrar = (descripcion) => {

    guardarDB();

    let nuevoListado = listadoPorHacer.filter(tarea =>
        tarea.descripcion !== descripcion)

    if (listadoPorHacer.length === nuevoListado.length) {
        return false;
    } else {
        listadoPorHacer = nuevoListado;
        guardarDB();
        return true;
    }
}

module.exports = {
    crear,
    getListado,
    actualizar,
    borrar
}